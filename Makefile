FLG1 := -std=c++20 -O3 -march=native -mtune=native -fiopenmp -DNDEBUG -fno-math-errno -flto -static
FLG2 := -std=c++20 -O3 -march=broadwell -mtune=broadwell -fiopenmp -DNDEBUG -fno-math-errno -flto -static
OUT  := Schwinger
# INC  := ./Eigen_3.4
CC   := icpx


all:
# 		$(CC) -I $(INC) Schwinger.cpp $(FLG1) -o $(OUT)
		$(CC) Schwinger.cpp $(FLG1) -o $(OUT)
cluster:
# 		$(CC) -I $(INC) Schwinger.cpp $(FLG2) -o $(OUT)
		$(CC) Schwinger.cpp $(FLG2) -o $(OUT)
run:
		./$(OUT)
clean:
		rm $(OUT)
