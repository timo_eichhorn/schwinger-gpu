#ifndef LETTUCE_SMEARING_HPP
#define LETTUCE_SMEARING_HPP

// Non-standard library headers
#include "dressing.hpp"
#include "observables.hpp"
//----------------------------------------
// Standard library headers
// ...
//----------------------------------------
// Standard C++ headers
#include <complex>
//----------------------------------------
// Standard C headers
#include <cmath>

//----------------------------------------
// APE Smearing for the entire lattice

template<typename T, size_t Nx, size_t Nt>
void APESmearing(const GaugeField<T, Nx, Nt>& U, GaugeField<T, Nx, Nt>& U_smeared, const T alpha) noexcept
{
    // #pragma omp parallel for
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    for (size_t mu = 0; mu < 2; ++mu)
    {
        U_smeared[t][x][mu] = std::arg((1.0 - alpha) * std::exp(i<T> * U[t][x][mu]) + 0.5 * alpha * Staple(U, t, x, mu));
    }
}

//----------------------------------------
// APE Smearing for a single link at (t, x, mu)

template<typename T, size_t Nx, size_t Nt>
[[nodiscard]]
T APESmearingSingle(const GaugeField<T, Nx, Nt>& U, const T alpha, const int t, const int x, const int mu) noexcept
{
    return std::arg((1.0 - alpha) * std::exp(i<T> * U[t][x][mu]) + 0.5 * alpha * Staple(U, t, x, mu));
}

//----------------------------------------
// Stout Smearing for the entire lattice
// TODO: Check if this implementation is correct! For high smearing parameters, the action increases (perhaps the parameters are just too high)

template<typename T, size_t Nx, size_t Nt>
void StoutSmearing(const GaugeField<T, Nx, Nt>& U, GaugeField<T, Nx, Nt>& U_smeared, const T alpha) noexcept
{
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    for (int mu = 0; mu < 2; ++mu)
    {
        int nu {1 - mu};
        // std::complex<T> Omega {alpha * Staple(U, t, x, mu) * std::exp(-i<T> * U[t][x][mu])};
        // U_smeared[t][x][mu] = std::arg(std::exp(i<T> * std::imag(Omega)) * std::exp(i<T> * U[t][x][mu]));
        U_smeared[t][x][mu] = U[t][x][mu] + alpha * (std::sin(U[t][x][nu] + U[(t+mu)%Nt][(x+nu)%Nx][mu] - U[(t+nu)%Nt][(x+mu)%Nx][nu] - U[t][x][mu]) + std::sin(-U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U[t][x][mu]));
        // std::arg(std::exp(i<T> * alpha * Staple(U, t, x, mu)) * std::exp(-i<T> * U[t][x][mu]));
        // std::arg((1.0 - alpha) * std::exp(i<T> * U[t][x][mu]) + 0.5 * alpha * Staple(U, t, x, mu));
    }
}

#endif
