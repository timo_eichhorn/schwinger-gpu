#ifndef LETTUCE_METADYNAMICS_HPP
#define LETTUCE_METADYNAMICS_HPP

// Non-standard library headers
#include "dressing.hpp"
//----------------------------------------
// Standard library headers
// ...
//----------------------------------------
// Standard C++ headers
#include <fstream>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <iterator>
//----------------------------------------
// Standard C headers
#include <cassert>
#include <cmath>

//-------------------------------------------------------------------------------------

// Metadynamics bias potential
// TODO: Add penalty weight range as parameter instead of function so we can load and plot the potential properly

template<typename floatT>
struct MetaBiasPotential
{
    // Histogram counts generalized to the linearly interpolating histogram used here
    std::vector<floatT> bin_count;
    // Minimum and maximum values of the topological charge histogram
    floatT Q_min, Q_max;
    // Number of bins and number of counts/edges (since this is a interpolating histogram)
    size_t bin_number;
    size_t edge_number;
    // Bin width of the histogram
    // Weight w that controls how fast the histogram grows
    // Threshold weight k that disencourages topological charges outside the interval [Q_min, Q_max)
    floatT bin_width, bin_width_inverse, weight, threshold_weight;
    // Count for how often the topological charge takes on values outside the interval [Q_min, Q_max)
    uint_fast64_t exceeded_count;
    // Stream for saving histogram to file
    std::ofstream binlog;
    // Stream for loading histogram from file
    std::ifstream binload;

    MetaBiasPotential(const floatT Q_min_in, const floatT Q_max_in, const size_t bin_number_in, const floatT weight_in, const floatT threshold_weight_in)
    {
        assert(Q_min_in < Q_max_in);
        Q_min = Q_min_in;
        Q_max = Q_max_in;
        bin_number = bin_number_in;
        edge_number = bin_number + 1;
        bin_width = (Q_max - Q_min) / bin_number;
        bin_width_inverse = 1.0 / bin_width;
        weight = weight_in;
        threshold_weight = threshold_weight_in;
        // Resize bin_count and set all entries to 0
        bin_count.assign(edge_number, 0.0);
        // Reset out of range count
        exceeded_count = 0;
        // Print message with parameters
        std::cout << "\nInitialized MetaBiasPotential with the following parameters:\n";
        std::cout << "  Q_min: " << Q_min << "\n";
        std::cout << "  Q_max: " << Q_max << "\n";
        std::cout << "  bin_number: " << bin_number << "\n";
        std::cout << "  edge_number: " << edge_number << "\n";
        std::cout << "  bin_width: " << bin_width << "\n";
        std::cout << "  bin_width_inverse: " << bin_width_inverse << "\n";
        std::cout << "  weight: " << weight << "\n";
        std::cout << "  threshold_weight: " << threshold_weight << "\n";
        std::cout << "  exceeded_count: " << exceeded_count << "\n" << std::endl;
    }
    
    // MetaBiasPotential(const floatT Q_min_in, const floatT Q_max_in, const size_t bin_number_in, const floatT weight_in, const floatT threshold_weight_in):
    //     bin_number = (bin_number_in),
    //     Q_min (Q_min_in),
    //     Q_max (Q_max_in)
    //     bin_width = {}
    // MetaBiasPotential(const floatT Q_max_in):
    //     Q_min (Q_min_in){}

    //-----
    // Function that initializes a bias potential based on Q range and weight

    // void InitializeBiasPotential(const floatT Q_min_in, const floatT Q_max_in, const size_t bin_number_in, const floatT weight_in, const floatT threshold_weight_in)
    // {
    //     assert(Q_min_in < Q_max_in);
    //     Q_min = Q_min_in;
    //     Q_max = Q_max_in;
    //     bin_number = bin_number_in;
    //     bin_width = (Q_max - Q_min) / bin_number;
    //     bin_width_inverse = 1.0 / bin_width;
    //     std::cout << "\nInitialized MetaBiasPotential with the following parameters:\n";
    //     std::cout << "  Q_min: " << Q_min << "\n";
    //     std::cout << "  Q_max: " << Q_max << "\n";
    //     std::cout << "  bin_number: " << bin_number << "\n";
    //     std::cout << "  bin_width: " << bin_width << "\n" << std::endl;
    //     weight = weight_in;
    //     threshold_weight = threshold_weight_in;
    //     // Resize bin_count and set all entries to 0
    //     // bin_count.resize(bin_number);
    //     // std::fill(bin_count.begin(), bin_count.end(), 0.0);
    //     bin_count.assign(bin_number, 0.0);
    //     // Reset out of range count
    //     exceeded_count = 0;
    // }

    //-----
    // Function that updates the histogram
    // TODO: Add additional weight parameter to accelerate the creation of the potential for topological updates?

    void UpdatePotential(const floatT Q) noexcept
    {
        int bin_index = std::floor((Q - Q_min) * bin_width_inverse);
        // Effectively means that our potential is limited to the interval [Q_min, Q_max)
        // if ((unsigned int)(bin_index) <= (unsigned int)(bin_number - 1))
        // if ((unsigned int)(bin_index) < (unsigned int)(bin_number - 1))
        // if ((unsigned int)(bin_index) < (unsigned int)(edge_number - 1))
        if (static_cast<unsigned int>(bin_index) < static_cast<unsigned int>(edge_number - 1))
        {
            floatT interpolation_constant = (Q - (Q_min + bin_index * bin_width)) * bin_width_inverse;
            bin_count[bin_index] += weight * (1.0 - interpolation_constant);
            bin_count[bin_index + 1] += weight * interpolation_constant;
        }
        else
        {
            exceeded_count += 1;
        }
    }

    //-----
    // Function that returns the histogram entry

    floatT ReturnPotential(const floatT Q) const noexcept
    {
        int bin_index = std::floor((Q - Q_min) * bin_width_inverse);
        // Effectively means that our potential is limited to the interval [Q_min, Q_max)
        // if ((unsigned int)(bin_index) <= (unsigned int)(bin_number - 1))
        // if ((unsigned int)(bin_index) < (unsigned int)(bin_number - 1))
        // if ((unsigned int)(bin_index) < (unsigned int)(edge_number - 1))
        if (static_cast<unsigned int>(bin_index) < static_cast<unsigned int>(edge_number - 1))
        {
            floatT interpolation_constant = (Q - (Q_min + bin_index * bin_width)) * bin_width_inverse;
            return bin_count[bin_index] * (1.0 - interpolation_constant) + interpolation_constant * bin_count[bin_index + 1];
        }
        else
        {
            // Original version
            // return threshold_weight * std::min(std::pow((Q - Q_min), 2), std::pow((Q - Q_max), 2));
            // Second version
            // Having both differences is redundant for symmetric potentials, but this way it works for arbitrary potentials
            // return threshold_weight * std::min(std::abs(Q * Q - Q_min * Q_min), std::abs(Q * Q - Q_max * Q_max));
            // Third, most extreme version
            return threshold_weight * (0.1 + std::min(std::pow((Q - Q_min), 2), std::pow((Q - Q_max), 2)));
        }
    }

    //-----
    // Create a penalty weight starting for values below Q_lower and values above Q_upper

    void AddPenaltyWeight(const floatT Q_lower, const floatT Q_upper, const std::string& filename)
    {
        assert(Q_lower < Q_upper);
        int lower_index = std::floor((Q_lower - Q_min) * bin_width_inverse);
        int upper_index = std::floor((Q_upper - Q_min) * bin_width_inverse);
        // for (int ind = 0; ind < lower_index; ++ind)
        // {
        //     floatT Q = Q_min + ind * bin_width;
        //     bin_count[ind] += threshold_weight * std::pow((Q - Q_lower), 2);
        // }
        // for (int ind = upper_index; ind < edge_number; ++ind)
        // {
        //     floatT Q = Q_min + ind * bin_width;
        //     bin_count[ind] += threshold_weight * std::pow((Q - Q_upper), 2);
        // }
        std::vector<floatT> penalty_potential(bin_count.size(), static_cast<floatT>(0.0));
        for (int ind = 0; ind < lower_index; ++ind)
        {
            floatT Q = Q_min + ind * bin_width;
            penalty_potential[ind] += threshold_weight * std::pow((Q - Q_lower), 2);
        }
        for (int ind = upper_index; ind < edge_number; ++ind)
        {
            floatT Q = Q_min + ind * bin_width;
            penalty_potential[ind] += threshold_weight * std::pow((Q - Q_upper), 2);
        }
        std::transform(bin_count.begin(), bin_count.end(), penalty_potential.begin(), bin_count.begin(), std::plus<floatT>());
        binlog.open(filename, std::fstream::out | std::fstream::app);
        std::copy(penalty_potential.cbegin(), std::prev(penalty_potential.cend()), std::ostream_iterator<floatT>(binlog, ","));
        binlog << bin_count.back() << "\n";
        binlog.close();
        binlog.clear();
    }

    void SubtractPenaltyWeight(const floatT Q_lower, const floatT Q_upper)
    {
        assert(Q_lower < Q_upper);
        int lower_index = std::floor((Q_lower - Q_min) * bin_width_inverse);
        int upper_index = std::floor((Q_upper - Q_min) * bin_width_inverse);
        for (int ind = 0; ind < lower_index; ++ind)
        {
            floatT Q = Q_min + ind * bin_width;
            bin_count[ind] -= threshold_weight * std::pow((Q - Q_lower), 2);
        }
        for (int ind = upper_index; ind < edge_number; ++ind)
        {
            floatT Q = Q_min + ind * bin_width;
            bin_count[ind] -= threshold_weight * std::pow((Q - Q_upper), 2);
        }
    }

    //-----
    // Function that saves the parameters to a file

    void SaveMetaParameters(const std::string& filename, const bool overwrite = false)
    {
        if (overwrite)
        {
            binlog.open(filename, std::fstream::out | std::fstream::trunc);
        }
        else
        {
            binlog.open(filename, std::fstream::out | std::fstream::app);
        }
        binlog << program_version << "\n";
        binlog << "Metadynamics Potential" << "\n";
        binlog << "Q_min: " << Q_min << "\n";
        binlog << "Q_max: " << Q_max << "\n";
        // TODO: Write penalty potential parameters to file?
        // binlog << "Q_pen_min: " << Q_pen_min << "\n";
        // binlog << "Q_pen_max: " << Q_pen_max << "\n";
        binlog << "bin_number: " << bin_number << "\n";
        binlog << "weight: " << weight << "\n";
        binlog << "threshold_weight: " << threshold_weight << "\n";
        binlog << "END_METADYN_PARAMS" << "\n";
        binlog.close();
        binlog.clear();
    }

    //-----
    // Function that saves the histogram to a file

    void SaveMetaPotential(const std::string& filename, const bool overwrite = false)
    {
        if (overwrite)
        {
            binlog.open(filename, std::fstream::out | std::fstream::trunc);
        }
        else
        {
            binlog.open(filename, std::fstream::out | std::fstream::app);
        }
        std::copy(bin_count.cbegin(), std::prev(bin_count.cend()), std::ostream_iterator<floatT>(binlog, ","));
        binlog << bin_count.back() << "\n";
        binlog << "exceeded_count: " << exceeded_count << "\n";
        binlog.close();
        binlog.clear();
    }

    //-----
    // C++ strings suck, so define this helper function
    // TODO: Eventually remove this function from this struct and move into utility library

    void LeftErase(std::string& str, const std::string& erase)
    {
        size_t pos = str.find(erase);
        if (pos != std::string::npos)
        {
            str.erase(pos, erase.length());
        }
    }

    //-----
    // Search the string 'str' for 'erase' starting and delete everything to the left of 'erase'
    // If including == true, erase everything including 'erase', otherwise only until 'erase'

    void EraseUntil(std::string& str, const std::string& erase, const bool including = true)
    {
        size_t pos = str.find(erase);
        if (pos != std::string::npos)
        {
            if (including)
            {
                str.erase(0, pos + erase.length());
            }
            else
            {
                str.erase(0, pos);
            }
        }
    }

    //-----
    // Function that loads the histogram parameters and the histogram itself from a file
    // TODO: There still seems to be a bug that causes the exceeded count line to be read into the last bin entry?
    // TODO: Work with string_view instead of strings?

    void LoadPotential(const std::string& filename)
    {
        binload.open(filename, std::fstream::in);
        std::string current_line;
        // Count the total linenumber using std::count (add one to the final result since we only counted the number of '\n' characters)
        size_t linecount = std::count(std::istreambuf_iterator<char>(binload), std::istreambuf_iterator<char>(), '\n') + 1;
        // Return to beginning of file
        binload.clear();
        binload.seekg(0, binload.beg);
        // Start reading parameters
        std::getline(binload, current_line);
        if (current_line == program_version)
        {
            std::cout << "Loading metadynamics potential from: " << filename << "\n";
            std::cout << "The file has " << linecount << " lines in total." << std::endl;
            // Skip second line
            std::getline(binload, current_line);
            // Get Q_min
            std::getline(binload, current_line);
            LeftErase(current_line, "Q_min: ");
            Q_min = std::stod(current_line);
            // Get Q_max
            std::getline(binload, current_line);
            LeftErase(current_line, "Q_max: ");
            Q_max = std::stod(current_line);
            // TODO: Read penalty potential parameters from file?
            // Get Q_pen_min
            // std::getline(binload, current_line);
            // LeftErase(current_line, "Q_pen_min: ");
            // Q_pen_min = std::stod(current_line);
            // Get Q_pen_max
            // std::getline(binload, current_line);
            // LeftErase(current_line, "Q_pen_max: ");
            // Q_pen_max = std::stod(current_line);
            // Get bin_number
            std::getline(binload, current_line);
            LeftErase(current_line, "bin_number: ");
            bin_number = std::stoi(current_line);
            // Get weight
            std::getline(binload, current_line);
            LeftErase(current_line, "weight: ");
            weight = std::stod(current_line);
            // Get threshold_weight
            std::getline(binload, current_line);
            LeftErase(current_line, "threshold_weight: ");
            threshold_weight = std::stod(current_line);
            // Iterate to the last two lines of the file (linecount - 3, since we still want to read in the second to last line)
            binload.clear();
            binload.seekg(0, binload.beg);
            for (size_t ind = 0; ind < linecount - 3; ++ind)
            {
                binload.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            // Load histogram into bin_count
            bin_count.clear();
            while(std::getline(binload, current_line, ','))
            {
                bin_count.push_back(std::stod(current_line));
            }
            // Get exceeded count
            std::getline(binload, current_line);
            EraseUntil(current_line, "exceeded_count: ", true);
            exceeded_count = std::stoull(current_line);
            // Calculate secondary parameters from primary parameters
            // TODO: Use Init function?
            bin_width = (Q_max - Q_min) / bin_number;
            bin_width_inverse = 1.0 / bin_width;
            edge_number = bin_number + 1;
            // Print message with parameters
            std::cout << "\nSuccessfully loaded MetaBiasPotential from " << filename << " with the following parameters:\n";
            std::cout << "  Q_min: " << Q_min << "\n";
            std::cout << "  Q_max: " << Q_max << "\n";
            std::cout << "  bin_number: " << bin_number << "\n";
            std::cout << "  edge_number: " << edge_number << "\n";
            // TODO: Remove next line after testing
            std::cout << "  bin_count.size(): " << bin_count.size() << "\n";
            std::cout << "  bin_width: " << bin_width << "\n";
            std::cout << "  bin_width_inverse: " << bin_width_inverse << "\n";
            std::cout << "  weight: " << weight << "\n";
            std::cout << "  threshold_weight: " << threshold_weight << "\n";
            std::cout << "  exceeded_count: " << exceeded_count << "\n" << std::endl;
        }
        else
        {
            std::cout << "Metadynamics potential file comes from an incompatible program version!\n";
            std::cout << "Current version: " << program_version << "\n";
            std::cout << "File version: " << current_line << "\n";
        }
        binload.close();
        binload.clear();
    }
};

#endif
