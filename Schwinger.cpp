// Schwinger model
// Debug flags:

// Non-standard library headers
#include "dressing.hpp"
#include "metadynamics.hpp"
#include "observables.hpp"
#include "tests.hpp"
#include "pcg_random.hpp"
//----------------------------------------
// Standard library headers
#include <omp.h>
//----------------------------------------
// Standard C++ headers
#include <algorithm>
#include <array>
#include <chrono>
#include <complex>
#include <experimental/iterator>
#include <filesystem>
#include <fstream>
// #include <queue>
#include <random>
#include <string>
#include <utility>
#include <vector>
#include <iomanip>
#include <iostream>
#include <iterator>
//----------------------------------------
// Standard C headers
#include <cmath>
#include <ctime>

//----------------------------------------
// Using-declarations and type aliases
using std::cin;
using std::cout;
using std::endl;
using std::to_string;
using std::conj;
using std::array;
using std::vector;

//-------------------------------------------------------------------------------------

//----------------------------------------
// Function to get user input with error handling

template<typename T>
void ValidatedIn(const std::string& message, T& target)
{
    // Count so that this function doesn't spam the entire cluster hard drive again
    size_t count {0};
    while(std::cout << let::col::boldblue << message << let::col::reset << "\n" && !(std::cin >> target) && count < 10)
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << let::col::red << "Invalid input." << let::col::reset << "\n";
        ++count;
    }
}

//----------------------------------------
// Receives simulation parameters from user input

void Configuration()
{
    cout << let::col::boldblue << "\n+------------------------------------------------+\n";
    cout << std::left << std::setw(49) << "| U(1) theory simulation" << "|\n";
    // cout << "|Current version: " << std::setw(49) << program_version << "|\n";
    cout << std::left << std::setw(49) << "| Current version: " + program_version << "|\n";
    cout << "+------------------------------------------------+\n\n" << let::col::reset;
    // Get simulation parameters from user input
    cout << let::col::boldblue << "Getting input parameters" << let::col::reset << "\n";
    ValidatedIn("Please enter beta (initial value): ", beta);
    ValidatedIn("Please enter n_run_total: ", n_run_total);
    ValidatedIn("Please enter n_run_f (set to 0 if multiscale is disabled): ", n_run_f);
    ValidatedIn("Please enter n_run_c (set to 0 if multiscale is disabled): ", n_run_c);
    ValidatedIn("Please enter expectation_period: ", expectation_period);
    ValidatedIn("Please enter top_update (1 or 0): ", top_update);

    cout << "\n" << "beta is " << beta << ".\n";
    cout << "The chosen lattice size (Nt x Nx) is " << to_string(Nt) + " x " + to_string(Nx) << ", n_run_total is " << n_run_total << ", n_run_f is " << n_run_f << ", n_run_c is " << n_run_c << ", and expectation_period is " << expectation_period << ".\n";
    if (top_update)
    {
        cout << let::col::green << "Topological updates are enabled." << let::col::reset << "\n";
    }
    else
    {
        cout << let::col::red << "Topological updates are disabled." << let::col::reset << "\n";
    }
    if constexpr(metadynamics_enabled)
    {
        cout << let::col::green << "Metadynamics updates are enabled." << let::col::reset << "\n";
        if constexpr(metapotential_updated)
        {
            cout << let::col::green << "Updates of the metapotential are enabled." << let::col::reset << "\n";
        }
        else
        {
            cout << let::col::red << "Updates of the metapotential are disabled." << let::col::reset << "\n";
        }
    }
    else
    {
        cout << let::col::red << "Metadynamics updates are disabled." << let::col::reset << "\n";
    }
    if constexpr(multiscale_enabled)
    {
        cout << let::col::green << "Multiscale updates are enabled." << let::col::reset << "\n";
    }
    else
    {
        cout << let::col::red << "Multiscale updates are disabled." << let::col::reset << "\n";
    }

    //-----
    // Active flags

    #if defined(DEBUG_MODE_TERMINAL) || defined(DEBUG_MODE_LOGFILE) || defined(FIXED_SEED)
    cout << "Active compile flags:\n";
    #else
    cout << "No custom compile flags active.\n";
    #endif
    // #ifdef DEBUG_MODE_TERMINAL
    // cout << "DEBUG_MODE_TERMINAL\n";
    // #endif
    // #ifdef DEBUG_MODE_LOGFILE
    // cout << "DEBUG_MODE_LOGFILE\n";
    // #endif
    #ifdef FIXED_SEED
    cout << "FIXED_SEED\n";
    #endif
    #if defined(DEBUG_MODE_TERMINAL) || defined(DEBUG_MODE_LOGFILE) || defined(FIXED_SEED)
    cout << "\n";
    #endif
    cout << "\n--------------------------------------------------\n\n";
}

//----------------------------------------
// Writes simulation parameters to files

void SaveParameters(const std::string& filename, const std::string& starttimestring)
{
    datalog.open(filename, std::fstream::out | std::fstream::app);
    datalog << program_version << "\n";
    datalog << "logfile\n\n";
    #ifdef DEBUG_MODE_TERMINAL
    datalog << "DEBUG_MODE_TERMINAL\n";
    #endif
    datalog << starttimestring << "\n";
    datalog << "Nt = " << Nt << "\n";
    datalog << "Nx = " << Nx << "\n";
    datalog << "beta = " << beta << "\n";
    datalog << "metadynamics_enabled = " << metadynamics_enabled << "\n";
    datalog << "metapotential_updated = " << metapotential_updated << "\n";
    datalog << "multiscale_enabled = " << multiscale_enabled << "\n";
    datalog << "n_run_total = " << n_run_total << "\n";
    datalog << "n_run_f = " << n_run_f << "\n";
    datalog << "n_run_c = " << n_run_c << "\n";
    datalog << "expectation_period = " << expectation_period << "\n";
    datalog << "n_smear = " << n_smear << "\n";
    datalog << "multi_hit = " << multi_hit << "\n";
    datalog << "top_update = " << top_update << "\n";
    datalog << "n_smooth = " << n_smooth << "\n";
    datalog << "END_PARAMS" << "\n" << endl;
    datalog.close();
    datalog.clear();
}

//----------------------------------------
// Creates directories and files to store data

void CreateFiles()
{
    // Directory name appendix (string)
    std::string appendString;
    // Directory name (prefix)
    std::string directoryname_pre;
    // Directory name
    std::string directoryname;

    directoryname_pre = "Schwinger_N=" + to_string(Nt) + "x" + to_string(Nx) + "_beta=" + to_string(beta);
    directoryname = directoryname_pre;
    int append = 1;

    while (std::filesystem::exists(directoryname))
    {
        appendString = to_string(append);
        directoryname = directoryname_pre + " (" + appendString + ")";
        ++append;
    }

    std::filesystem::create_directory(directoryname);
    cout << "Created directory \"" << directoryname << "\".\n";
    logfilepath = directoryname + "/log.txt";
    parameterfilepath = directoryname + "/parameters.txt";
    multiscalefilepath = directoryname + "/multiscale.txt";
    metapotentialfilepath = directoryname + "/metapotential.txt";
    penaltypotentialfilepath = directoryname + "/penaltypotential.txt";
    flowfilepath = directoryname + "/wilsonflow.txt";
    cout << let::col::boldblue << "Filepath (log):\t\t\t" << logfilepath << let::col::reset << "\n";
    cout << let::col::boldblue << "Filepath (param):\t\t" << parameterfilepath << let::col::reset << "\n";
    cout << let::col::boldblue << "Filepath (multiscale):\t\t" << multiscalefilepath << let::col::reset << "\n";
    cout << let::col::boldblue << "Filepath (metadyn):\t\t" << metapotentialfilepath << let::col::reset << "\n";
    cout << let::col::boldblue << "Filepath (penalty):\t\t" << penaltypotentialfilepath << let::col::reset << "\n";
    cout << let::col::boldblue << "Filepath (flow):\t\t" << flowfilepath << let::col::reset << "\n\n";
    #ifdef DEBUG_MODE_TERMINAL
    cout << "DEBUG_MODE_TERMINAL\n\n";
    #endif

    //-----
    // Writes parameters to files

    std::time_t start_time = std::chrono::system_clock::to_time_t(start);
    std::string start_time_string = std::ctime(&start_time);

    // Logfile

    SaveParameters(logfilepath, start_time_string);

    // Parameterfile

    SaveParameters(parameterfilepath, start_time_string);

    // Multiscalelogfile

    if constexpr(multiscale_enabled)
    {
        SaveParameters(multiscalefilepath, start_time_string);
    }

    // Wilson flow logfile

    SaveParameters(flowfilepath, start_time_string);

    // Flush cout
    cout << std::flush;
}

//----------------------------------------
// Print final

template<typename T>
void PrintFinal(std::ostream& log, const AcceptanceRates& AcceptanceRates_f, const AcceptanceRates& AcceptanceRates_c, const T epsilon_f, const T epsilon_c, const std::time_t& end_time, const std::chrono::duration<double>& elapsed_seconds)
{
    double top_update_norm_f {1.0};
    double top_update_norm_c {1.0};
    if constexpr(multiscale_enabled)
    {
        top_update_norm_f = 1.0 / static_cast<double>(n_run_c/expectation_period * n_run_f);
        top_update_norm_c = 1.0 / static_cast<double>(n_run_c);
    }
    else
    {
        top_update_norm_f = 1.0 / n_run_total;
        top_update_norm_c = 1.0 / n_run_total;
    }

    log << "metro_acceptance_f: " << AcceptanceRates_f.metro_acceptance * update_norm_f << "\n";
    log << "metro_acceptance_c: " << AcceptanceRates_c.metro_acceptance * update_norm_c << "\n";
    log << "top_acceptance_f: " << AcceptanceRates_f.top_acceptance * top_update_norm_f << "\n";
    log << "top_acceptance_c: " << AcceptanceRates_c.top_acceptance * top_update_norm_c << "\n";
    log << "epsilon_f: " << epsilon_f << "\n";
    log << "epsilon_c: " << epsilon_c << "\n";
    log << std::ctime(&end_time) << "\n";
    log << "Required time: " << elapsed_seconds.count() << "s\n";
}

//----------------------------------------
// Create PRNGs
// TODO: Probably should make this a template and use concepts to constrain the argument properly

[[nodiscard]]
vector<pcg64> CreatePRNGs(const int thread_num = 0)
{
    vector<pcg64> temp_vec;
    #if defined(_OPENMP)
        int max_thread_num = omp_get_max_threads();
    #else
        int max_thread_num = 1;
    #endif
    cout << "Maximum number of threads: " << max_thread_num << endl;
    #if defined(_OPENMP)
        if (thread_num != 0)
        {
            max_thread_num = thread_num;
            omp_set_num_threads(thread_num);
        }
    #endif
    if (max_thread_num != 1)
    {
        cout << "Creating PRNG vector with " << max_thread_num << " PRNGs.\n" << endl;
    }
    else
    {
        cout << "Creating PRNG vector with " << max_thread_num << " PRNG.\n" << endl;
    }
    for (int ind = 0; ind < max_thread_num; ++ind)
    {
        #ifdef FIXED_SEED
        pcg64 generator_rand_temp(i);
        temp_vec.emplace_back(generator_rand_temp);
        // temp_vec.emplace_back(generator_rand_temp(i));
        #else
        pcg_extras::seed_seq_from<std::random_device> seed_source_temp;
        pcg64 generator_rand_temp(seed_source_temp);
        temp_vec.emplace_back(generator_rand_temp);
        // temp_vec.emplace_back(generator_rand_temp(seed_source_temp));
        #endif
    }
    return temp_vec;
}

//----------------------------------------
// Cold start

template<typename T, size_t Nx, size_t Nt>
void ColdStart(GaugeField<T, Nx, Nt>& U) noexcept
{
    for (auto &timeslice : U)
    for (auto &spaceslice : timeslice)
    {
        spaceslice.fill(T(0.0));
    }
    cout << "\n+------------------------------------------------+\n";
    cout << std::left << std::setw(49) << "| Cold start (" + to_string(Nt) + " x " + to_string(Nx) + ")" << "|\n";
    cout << std::left << std::setw(49) << "| Plaq (Re): " + to_string(std::real(Plaquette(U))) << "|\n";
    cout << std::left << std::setw(49) << "| TopCharge: " + to_string(TopologicalCharge(U)) << "|\n";
    cout << "+------------------------------------------------+\n\n";
}

//----------------------------------------
// Hot start

template<typename T, size_t Nx, size_t Nt>
void HotStart(GaugeField<T, Nx, Nt>& U) noexcept
{
    std::uniform_real_distribution<T> distribution_phase(0, 2*pi<T>);
    auto gen = [&distribution_phase](){return distribution_phase(generator_rand);};

    for (auto &timeslice : U)
    for (auto &spaceslice : timeslice)
    {
        std::generate(spaceslice.begin(), spaceslice.end(), gen);
    }
    double Q = TopologicalCharge(U);
    cout << "\n+------------------------------------------------+\n";
    cout << std::left << std::setw(49) << "| Hot start (" + to_string(Nt) + " x " + to_string(Nx) + ")" << "|\n";
    cout << std::left << std::setw(49) << "| Plaq (Re): " + to_string(std::real(Plaquette(U))) << "|\n";
    cout << std::left << std::setw(49) << "| TopCharge: " + to_string(TopologicalCharge(U)) << "|\n";
    cout << "+------------------------------------------------+\n\n";
}

//----------------------------------------
// Instanton configuration with topological charge Q

template<typename T, size_t Nx, size_t Nt>
void InstantonStart(GaugeField<T, Nx, Nt>& U, const int Q) noexcept
{
    // Reset entire field to zero
    ColdStart(U);
    // Set field to zero on all links in space direction and change field on all links in time direction
    T Field_t = 2.0 * pi<T> * Q / (Nx * Nt);
    T Field_x = 2.0 * pi<T> * Q / Nt;
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        U[t][x][0] = -Field_t * x;
        U[t][x][1] = 0.0;
    }
    // Change field on last spaceslice
    for (size_t t = 0; t < Nt; ++t)
    {
        U[t][Nx - 1][1] = Field_x * t;
    }
    cout << "\n+------------------------------------------------+\n";
    cout << std::left << std::setw(49) << "| Instanton start (" + to_string(Nt) + " x " + to_string(Nx) + ", Q = " + to_string(Q) + ")" << "|\n";
    cout << std::left << std::setw(49) << "| Plaq (Re): " + to_string(std::real(Plaquette(U))) << "|\n";
    cout << std::left << std::setw(49) << "| TopCharge: " + to_string(TopologicalCharge(U)) << "|\n";
    cout << "+------------------------------------------------+\n\n";
}

//----------------------------------------
// Resets the phases of the fields to be in the interval [0, 2*pi)
// TODO: Why not just project to (-2*pi, 2*pi) with single fmod?

template<typename T, size_t Nx, size_t Nt>
void ResetPhase(GaugeField<T, Nx, Nt>& U) noexcept
{
    // #pragma omp parallel for
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    for (size_t mu = 0; mu < 2; ++mu)
    {
        // U[t][x][mu] = std::fmod(std::fmod(U[t][x][mu], 2.0 * pi<T>) + 3.0 * pi<T>, 2.0 * pi<T>) - pi<T>;
        U[t][x][mu] = std::fmod(U[t][x][mu], 2.0 * pi<T>);
    }
}

//----------------------------------------
// Metropolis update

template<typename T, size_t Nx, size_t Nt>
void MetropolisUpdate(GaugeField<T, Nx, Nt>& U, const T beta, uint_fast64_t& metro_acceptance, std::uniform_real_distribution<T>& distribution_prob, std::uniform_real_distribution<T>& distribution_phase) noexcept
{
    // TODO: Check for correctness
    // TODO: Create threads early, and use omp single (?) for parts that should only be run once
    for (size_t mu = 0; mu < 2; ++mu)
    for (size_t eo = 0; eo < 2; ++eo)
    {
        #pragma omp parallel for reduction(+:metro_acceptance)
        for (size_t t = 0; t < Nt; ++t)
        {
            size_t offset = (t & 1) ^ eo;
            for (size_t x = offset; x < Nx; x += 2)
            {
                std::complex<T> St = StapleDaggered(U, t, x, mu);
                for (int n_hit = 0; n_hit < multi_hit; ++n_hit)
                {
                    #if defined(_OPENMP)
                        T Suggestion = U[t][x][mu] + distribution_phase(prng_vector[omp_get_thread_num()]);
                        T DeltaS = beta * std::real((std::exp(i<T> * U[t][x][mu]) - std::exp(i<T> * Suggestion)) * St);
                        T p = std::exp(-DeltaS);
                        T q = distribution_prob(prng_vector[omp_get_thread_num()]);
                    #else
                        T Suggestion = U[t][x][mu] + distribution_phase(generator_rand);
                        T DeltaS = beta * std::real((std::exp(i<T> * U[t][x][mu]) - std::exp(i<T> * Suggestion)) * St);
                        T p = std::exp(-DeltaS);
                        T q = distribution_prob(generator_rand);
                    #endif
                    if (q <= p)
                    {
                        U[t][x][mu] = Suggestion;
                        metro_acceptance += 1;
                    }
                }
            }
        }
    }
}

//----------------------------------------
// Metadynamics version of Metropolis update (Metropolis with additional metapotential)

template<typename T, size_t Nx, size_t Nt>
void MetadynamicsMetropolisUpdate(GaugeField<T, Nx, Nt>& U, MetaBiasPotential<T>& BiasPotential, const T beta, uint_fast64_t& metro_acceptance, std::uniform_real_distribution<T>& distribution_prob, std::uniform_real_distribution<T>& distribution_phase) noexcept
{
    T Q_old = TopologicalChargeMeta(U);
    // TODO: Add checkerboard pattern (can't parallelize this afaik)
    for (int mu = 0; mu < 2; ++mu)
    for (int t = 0; t < Nt; ++t)
    for (int x = 0; x < Nx; ++x)
    {
        int nu {1 - mu};
        T St_phase1 = -U[t][x][nu] - U[(t+mu)%Nt][(x+nu)%Nx][mu] + U[(t+nu)%Nt][(x+mu)%Nx][nu];
        T St_phase2 = U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] - U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] - U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu];
        std::complex<T> St = std::exp(i<T> * St_phase1) + std::exp(i<T> * St_phase2);
        for (int n_hit = 0; n_hit < multi_hit; ++n_hit)
        {
            T Suggestion = U[t][x][mu] + distribution_phase(generator_rand);
            T DeltaQ = ((1 - 2 * mu) / (2 * pi<T>)) * (std::sin(Suggestion + St_phase1) - std::sin(Suggestion + St_phase2) - std::sin(U[t][x][mu] + St_phase1) + std::sin(U[t][x][mu] + St_phase2));
            T DeltaS = beta * std::real((std::exp(i<T> * U[t][x][mu]) - std::exp(i<T> * Suggestion)) * St);
            T p = std::exp(-DeltaS + BiasPotential.ReturnPotential(Q_old) - BiasPotential.ReturnPotential(Q_old + DeltaQ));
            T q = distribution_prob(generator_rand);
            if (q <= p)
            {
                U[t][x][mu] = Suggestion;
                metro_acceptance += 1;
                Q_old += DeltaQ;
                if constexpr(metapotential_updated)
                {
                    BiasPotential.UpdatePotential(Q_old);
                }
            }
        }
    }
}

//----------------------------------------
// Overrelaxation update

template<typename T, size_t Nx, size_t Nt>
void OverrelaxationUpdate(GaugeField<T, Nx, Nt>& U) noexcept
{
    // TODO: How to parallelize? Probably the same way as Metropolis updates, since only the staple is involved
    for (size_t mu = 0; mu < 2; ++mu)
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        U[t][x][mu] = 2.0 * pi<T> - 2.0 * std::arg(StapleDaggered(U, t, x, mu)) - U[t][x][mu];
    }
}

//----------------------------------------
// Topological update (link-wise multiplication of old configuration with instanton with random topological charge +1 or -1)

template<typename T, size_t Nx, size_t Nt>
void TopologicalUpdate(GaugeField<T, Nx, Nt>& U, const T beta, uint_fast64_t& top_acceptance, std::uniform_real_distribution<T>& distribution_prob, std::uniform_int_distribution<int>& distribution_int) noexcept
{
    // Multiply +1 or -1 instanton configuration on old configuration
    GaugeField<T, Nx, Nt> U_new = U;
    int Q = distribution_int(generator_rand) * 2 - 1;
    T Field_t = 2.0 * pi<T> * Q / (Nx * Nt);
    T Field_x = 2.0 * pi<T> * Q / Nt;
    // Change field on all timesclices except the last
    #pragma omp parallel for
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        U_new[t][x][0] -= Field_t * x;
    }
    // Change field on last spaceslice
    for (size_t t = 0; t < Nt; ++t)
    {
        U_new[t][Nx - 1][1] += Field_x * t;
    }
    // Metropolis accept/reject step
    T DeltaS = WilsonAction(U_new, beta) - WilsonAction(U, beta);
    T p = std::exp(-DeltaS);
    T q = distribution_prob(generator_rand);
    if (q <= p)
    {
        U = U_new;
        top_acceptance += 1;
    }
}

//----------------------------------------
// Metadynamics version of Topological update (link-wise multiplication of old configuration with instanton with random topological charge +1 or -1)

template<typename T, size_t Nx, size_t Nt>
void MetadynamicsTopologicalUpdate(GaugeField<T, Nx, Nt>& U, MetaBiasPotential<T>& BiasPotential, const T beta, uint_fast64_t& top_acceptance, std::uniform_real_distribution<T>& distribution_prob, std::uniform_int_distribution<int>& distribution_int) noexcept
{
    // Multiply +1 or -1 instanton configuration on old configuration
    GaugeField<T, Nx, Nt> U_new = U;
    int Q = distribution_int(generator_rand) * 2 - 1;
    T Field_t = 2.0 * pi<T> * Q / (Nx * Nt);
    T Field_x = 2.0 * pi<T> * Q / Nt;
    // Change field on all timesclices except the last
    #pragma omp parallel for
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        U_new[t][x][0] -= Field_t * x;
    }
    // Change field on last timeslice
    for (size_t t = 0; t < Nt; ++t)
    {
        U_new[t][Nx - 1][1] += Field_x * t;
    }
    // Metropolis accept/reject step
    T Q_old = TopologicalChargeMeta(U);
    T Q_new = TopologicalChargeMeta(U_new);
    T DeltaS = WilsonAction(U_new, beta) - WilsonAction(U, beta);
    T p = std::exp(-DeltaS + BiasPotential.ReturnPotential(Q_old) - BiasPotential.ReturnPotential(Q_new));
    T q = distribution_prob(generator_rand);
    if (q <= p)
    {
        U = U_new;
        top_acceptance += 1;
        if constexpr(metapotential_updated)
        {
            BiasPotential.UpdatePotential(Q_new);
        }
    }
}

//----------------------------------------
// Update function

template<typename T, size_t Nx, size_t Nt>
void Update(GaugeField<T, Nx, Nt>& U, MetaBiasPotential<T>& BiasPotential, const T beta, const T epsilon, AcceptanceRates& AcceptanceRates, std::uniform_real_distribution<T>& distribution_prob, std::uniform_int_distribution<int>& distribution_int) noexcept
{
    std::uniform_real_distribution<T> distribution_phase(-epsilon, epsilon);
    AcceptanceRates.metro_acceptance = 0;

    // If metadynamics is enabled only use Metropolis and topological updates (if topological updates are enabled)
    if constexpr(metadynamics_enabled)
    {
        MetadynamicsMetropolisUpdate(U, BiasPotential, beta, AcceptanceRates.metro_acceptance, distribution_prob, distribution_phase);
        if (top_update)
        {
            MetadynamicsTopologicalUpdate(U, BiasPotential, beta, AcceptanceRates.top_acceptance, distribution_prob, distribution_int);
        }
    }
    // Otherwise additionally use overrelaxation Updates
    else
    {
        MetropolisUpdate(U, beta, AcceptanceRates.metro_acceptance, distribution_prob, distribution_phase);
        OverrelaxationUpdate(U);
        // TODO: Heatbath exact?
        if (top_update)
        {
            TopologicalUpdate(U, beta, AcceptanceRates.top_acceptance, distribution_prob, distribution_int);
        }
    }
    // TODO: Include phase reset with every update sweep?
    ResetPhase(U);
}

//----------------------------------------
// Wilson flow

template<Order order, WilsonFlowScheme scheme, typename T, size_t Nx, size_t Nt>
void WilsonFlow(GaugeField<T, Nx, Nt>& U, const T epsilon, const double precision = 1e-14) noexcept
{
    // Forwards sequential order
    if constexpr(order == Order::ForwardsSeq)
    {
        // Explicit schemes
        GaugeField<T, Nx, Nt> U_new = U;
        for (int t = 0; t < Nt; ++t)
        for (int x = 0; x < Nx; ++x)
        for (int mu = 0; mu < 2; ++mu)
        {
            if constexpr(scheme == WilsonFlowScheme::APE)
            {
                U[t][x][mu] = std::arg((1.0 - epsilon) * std::exp(i<T> * U[t][x][mu]) + 0.5 * epsilon * Staple(U, t, x, mu));
            }
            if constexpr(scheme == WilsonFlowScheme::Stout)
            {
                int nu {1 - mu};
                U[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U[t][x][nu] + U[(t+mu)%Nt][(x+nu)%Nx][mu] - U[(t+nu)%Nt][(x+mu)%Nx][nu] - U[t][x][mu]) + std::sin(-U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U[t][x][mu]));
            }
            if constexpr(scheme == WilsonFlowScheme::StoutNew)
            {
                int nu {1 - mu};
                U_new[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U[t][x][nu] + U[(t+mu)%Nt][(x+nu)%Nx][mu] - U[(t+nu)%Nt][(x+mu)%Nx][nu] - U[t][x][mu]) + std::sin(-U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U[t][x][mu]));
            }
            if constexpr(scheme == WilsonFlowScheme::RK4)
            {
                // TODO: Implement
            }
        }
        if constexpr(scheme == WilsonFlowScheme::StoutNew)
        {
            U = U_new;
        }

        //-----
        // Implicit schemes
        if constexpr(scheme == WilsonFlowScheme::EulerImplicit)
        {
            // Immediately use new links like Lüscher suggests
            GaugeField<T, Nx, Nt> U_new = U;
            for (int t = 0; t < Nt; ++t)
            for (int x = 0; x < Nx; ++x)
            for (int mu = 0; mu < 2; ++mu)
            {
                double diff {1.0};
                int nu {1 - mu};
                while (diff > precision)
                {
                    T Old_link = U_new[t][x][mu];
                    U_new[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U_new[t][x][nu] + U_new[(t+mu)%Nt][(x+nu)%Nx][mu] - U_new[(t+nu)%Nt][(x+mu)%Nx][nu] - U_new[t][x][mu]) + std::sin(-U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_new[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_new[t][x][mu]));
                    diff = std::abs(U_new[t][x][mu] - Old_link);
                    // std::cout << diff << "\n";
                    // ++count;
                }
                U[t][x][mu] = U_new[t][x][mu];
            }
        }
        if constexpr(scheme == WilsonFlowScheme::EulerImplicitNew)
        {
            // TODO: Check if implementation is correct
            // Update lattice without using the newly flowed links immediately
            GaugeField<T, Nx, Nt> U_new = U;
            GaugeField<T, Nx, Nt> U_temp = U;
            double diff {1.0};
            double norm {1.0 / (Nx * Nt * 2.0)};
            int count {0};
            while (diff > precision)
            {
                diff = 0.0;
                for (int t = 0; t < Nt; ++t)
                for (int x = 0; x < Nx; ++x)
                for (int mu = 0; mu < 2; ++mu)
                {
                    int nu {1 - mu};
                    U_temp[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U_new[t][x][nu] + U_new[(t+mu)%Nt][(x+nu)%Nx][mu] - U_new[(t+nu)%Nt][(x+mu)%Nx][nu] - U_new[t][x][mu]) + std::sin(-U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_new[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_new[t][x][mu]));
                    diff += std::abs(U_temp[t][x][mu] - U_new[t][x][mu]);
                    ++count;
                }
                diff *= norm;
                U_new = U_temp;
            }
            std::cout << count << "\n";
            U = U_new;
        }
        if constexpr(scheme == WilsonFlowScheme::MidpointImplicit)
        {
            // TODO: Check if implementation is correct
            GaugeField<T, Nx, Nt> U_mid = U;
            // GaugeField<T, Nx, Nt> U_new = U;
            for (int t = 0; t < Nt; ++t)
            for (int x = 0; x < Nx; ++x)
            for (int mu = 0; mu < 2; ++mu)
            {
                double diff {1.0};
                int nu {1 - mu};
                while (diff > precision)
                {
                    T Old_link = U_mid[t][x][mu];
                    U_mid[t][x][mu] = U[t][x][mu] + 0.5 * epsilon * (std::sin(U_mid[t][x][nu] + U_mid[(t+mu)%Nt][(x+nu)%Nx][mu] - U_mid[(t+nu)%Nt][(x+mu)%Nx][nu] - U_mid[t][x][mu]) + std::sin(-U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_mid[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_mid[t][x][mu]));
                    diff = std::abs(U_mid[t][x][mu] - Old_link);
                }
                U[t][x][mu] = U_mid[t][x][mu] + 0.5 * epsilon * (std::sin(U_mid[t][x][nu] + U_mid[(t+mu)%Nt][(x+nu)%Nx][mu] - U_mid[(t+nu)%Nt][(x+mu)%Nx][nu] - U_mid[t][x][mu]) + std::sin(-U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_mid[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_mid[t][x][mu]));
            }
        }
    }

    //----------------------------------------
    // Backwards sequential order
    if constexpr(order == Order::BackwardsSeq)
    {
        // Explicit schemes
        for (int t = Nt - 1; t >= 0; --t)
        for (int x = Nx - 1; x >= 0; --x)
        for (int mu = 1; mu >= 0; --mu)
        {
            if constexpr(scheme == WilsonFlowScheme::APE)
            {
                U[t][x][mu] = std::arg((1.0 - epsilon) * std::exp(i<T> * U[t][x][mu]) + 0.5 * epsilon * Staple(U, t, x, mu));
            }
            if constexpr(scheme == WilsonFlowScheme::Stout)
            {
                int nu {1 - mu};
                U[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U[t][x][nu] + U[(t+mu)%Nt][(x+nu)%Nx][mu] - U[(t+nu)%Nt][(x+mu)%Nx][nu] - U[t][x][mu]) + std::sin(-U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U[t][x][mu]));
            }
            if constexpr(scheme == WilsonFlowScheme::RK4)
            {
                // TODO: Implement
            }
        }

        //-----
        // Implicit schemes
        if constexpr(scheme == WilsonFlowScheme::EulerImplicit)
        {
            GaugeField<T, Nx, Nt> U_new = U;
            // Immediately use new links like Lüscher suggests
            for (int t = Nt - 1; t >= 0; --t)
            for (int x = Nx - 1; x >= 0; --x)
            for (int mu = 1; mu >= 0; --mu)
            {
                double diff {1.0};
                // int count {0};
                int nu {1 - mu};
                while (diff > precision)
                {
                    T Old_link = U_new[t][x][mu];
                    U_new[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U_new[t][x][nu] + U_new[(t+mu)%Nt][(x+nu)%Nx][mu] - U_new[(t+nu)%Nt][(x+mu)%Nx][nu] - U_new[t][x][mu]) + std::sin(-U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_new[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_new[t][x][mu]));
                    diff = std::abs(U_new[t][x][mu] - Old_link);
                    // std::cout << diff << "\n";
                    // ++count;
                }
                U[t][x][mu] = U_new[t][x][mu];
                // std::cout << count << "\n";
            }
        }
        if constexpr(scheme == WilsonFlowScheme::EulerImplicitNew)
        {
            // TODO: Check if implementation is correct
            // Update lattice without using the newly flowed links immediately
            GaugeField<T, Nx, Nt> U_new = U;
            GaugeField<T, Nx, Nt> U_temp = U;
            double diff {1.0};
            double norm {1.0 / (Nx * Nt * 2.0)};
            // int count {0};
            while (diff > precision)
            {
                diff = 0.0;
                for (int t = Nt - 1; t >= 0; --t)
                for (int x = Nx - 1; x >= 0; --x)
                for (int mu = 1; mu >= 0; --mu)
                {
                    int nu {1 - mu};
                    U_temp[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U_new[t][x][nu] + U_new[(t+mu)%Nt][(x+nu)%Nx][mu] - U_new[(t+nu)%Nt][(x+mu)%Nx][nu] - U_new[t][x][mu]) + std::sin(-U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_new[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_new[t][x][mu]));
                    diff += std::abs(U_temp[t][x][mu] - U_new[t][x][mu]);
                }
                diff *= norm;
                U_new = U_temp;
                // ++count;
            }
            // std::cout << count << "\n";
            U = U_new;
        }
        if constexpr(scheme == WilsonFlowScheme::MidpointImplicit)
        {
            // // TODO: Check if implementation is correct
            // GaugeField<T, Nx, Nt> U_mid = U;
            // // GaugeField<T, Nx, Nt> U_new = U;
            // for (int t = Nt - 1; t >= 0; --t)
            // for (int x = Nx - 1; x >= 0; --x)
            // for (int mu = 1; mu >= 0; --mu)
            // {
            //     double diff {1.0};
            //     int nu {1 - mu};
            //     while (diff > precision)
            //     {
            //         T Old_link = U_mid[t][x][mu];
            //         U_mid[t][x][mu] = U[t][x][mu] + 0.5 * epsilon * (std::sin(U_mid[t][x][nu] + U_mid[(t+mu)%Nt][(x+nu)%Nx][mu] - U_mid[(t+nu)%Nt][(x+mu)%Nx][nu] - U_mid[t][x][mu]) + std::sin(-U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_mid[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_mid[t][x][mu]));
            //         diff = std::abs(U_mid[t][x][mu] - Old_link);
            //     }
            //     U[t][x][mu] = U_mid[t][x][mu] + 0.5 * epsilon * (std::sin(U_mid[t][x][nu] + U_mid[(t+mu)%Nt][(x+nu)%Nx][mu] - U_mid[(t+nu)%Nt][(x+mu)%Nx][nu] - U_mid[t][x][mu]) + std::sin(-U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_mid[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_mid[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_mid[t][x][mu]));
            // }
            //-----
            GaugeField<T, Nx, Nt> U_new = U;
            GaugeField<T, Nx, Nt> U_temp = U;
            double diff {1.0};
            double norm {1.0 / (Nx * Nt * 2.0)};
            // int count {0};
            while (diff > precision)
            {
                diff = 0.0;
                for (int t = Nt - 1; t >= 0; --t)
                for (int x = Nx - 1; x >= 0; --x)
                for (int mu = 1; mu >= 0; --mu)
                {
                    int nu {1 - mu};
                    U_temp[t][x][mu] = U[t][x][mu] + 0.5 * epsilon * (std::sin(U_new[t][x][nu] + U_new[(t+mu)%Nt][(x+nu)%Nx][mu] - U_new[(t+nu)%Nt][(x+mu)%Nx][nu] - U_new[t][x][mu]) + std::sin(-U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_new[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_new[t][x][mu]));
                    diff += std::abs(U_temp[t][x][mu] - U_new[t][x][mu]);
                }
                diff *= norm;
                U_new = U_temp;
                // ++count;
            }
            // std::cout << count << "\n";
            for (int t = Nt - 1; t >= 0; --t)
            for (int x = Nx - 1; x >= 0; --x)
            for (int mu = 1; mu >= 0; --mu)
            {
                int nu {1 - mu};
                U[t][x][mu] = U[t][x][mu] + epsilon * (std::sin(U_new[t][x][nu] + U_new[(t+mu)%Nt][(x+nu)%Nx][mu] - U_new[(t+nu)%Nt][(x+mu)%Nx][nu] - U_new[t][x][mu]) + std::sin(-U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U_new[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U_new[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu] - U_new[t][x][mu]));
            }
        }
    }
}

//----------------------------------------
// Topological update using Wilson flow to improve acceptance rate

// template<Order order, WilsonFlowScheme scheme, typename T, size_t Nx, size_t Nt>
// void WilsonFlow(GaugeField<T, Nx, Nt>& U, const T epsilon, const double precision = 1e-14) noexcept

// template<typename T, size_t Nx, size_t Nt>
// void TopologicalUpdate(GaugeField<T, Nx, Nt>& U, const T beta, int& top_acceptance, std::uniform_real_distribution<T>& distribution_prob, std::uniform_int_distribution<int>& distribution_int) noexcept

template<Order order_fwd, WilsonFlowScheme scheme_fwd, Order order_bwd, WilsonFlowScheme scheme_bwd, typename T, size_t Nx, size_t Nt>
T TopologicalUpdateFlow(GaugeField<T, Nx, Nt>& U, const T beta, const T epsilon, const int n_flow, int& top_acceptance, std::uniform_real_distribution<T>& distribution_prob, int Q) noexcept
{
    GaugeField<T, Nx, Nt> U_new = U;
    T Old_Action = WilsonAction(U_new, beta);
    T jacobian_forward {0.0};
    T jacobian_backward {0.0};
    for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    {
        WilsonFlow<order_fwd, scheme_fwd>(U_new, epsilon);
        jacobian_forward += std::real(Plaquette(U_new));
    }
    // Multiply +1 or -1 instanton configuration on old configuration
    // int Q = distribution_int(generator_rand) * 2 - 1;
    T Field_t = 2.0 * pi<T> * Q / (Nx * Nt);
    T Field_x = 2.0 * pi<T> * Q / Nt;
    // Change field on all timesclices except the last
    #pragma omp parallel for
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        U_new[t][x][0] -= Field_t * x;
    }
    // Change field on last spaceslice
    for (size_t t = 0; t < Nt; ++t)
    {
        U_new[t][Nx - 1][1] += Field_x * t;
    }
    // Flow backwards
    for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    {
        WilsonFlow<order_bwd, scheme_bwd>(U_new, epsilon);
        jacobian_backward += std::real(Plaquette(U_new));
    }
    // Metropolis accept/reject step
    T DeltaS = WilsonAction(U_new, beta) - Old_Action;
    // The logarithm of the Jacobian is proportional to the negative integrated plaquette, so we get an additional factor jacobian_backward/jacobian_forward
    // The proportionality factors are the same and should cancel
    T p = std::exp(-DeltaS) * jacobian_backward/jacobian_forward;
    T q = distribution_prob(generator_rand);
    if (q <= p)
    {
        U = U_new;
        top_acceptance += 1;
    }
    // TODO: Should either modify DeltaS or also return the Jacobians
    return DeltaS;
}

//----------------------------------------
// Calculate and write observables to logfile

template<typename T, size_t Nx_f, size_t Nt_f, size_t Nx_c, size_t Nt_c>
void Observables(const GaugeField<T, Nx_f, Nt_f>& U_f, const GaugeField<T, Nx_c, Nt_c>& U_c, const MetaBiasPotential<T>& BiasPotential, const T beta, const int n_smear, std::ofstream& datalog, const int n_count)
{
    vector<T> WilsonAction_f(n_smear + 1);
    vector<T> Pl_Re_f(n_smear + 1);
    vector<T> Pl_Im_f(n_smear + 1);
    vector<std::complex<T>> Pl_f(n_smear + 1);
    vector<T> Q_f(n_smear + 1);
    vector<T> Q_squared_f(n_smear + 1);
    vector<T> Q_meta_f(n_smear + 1);
    double action_norm_f {1.0 / (Nt_f * Nx_f)};

    //-----
    // Unsmeared observables
    WilsonAction_f[0] = WilsonAction(U_f, beta) * action_norm_f;
    Pl_f[0] = Plaquette(U_f);
    Q_f[0] = TopologicalCharge(U_f);
    Q_meta_f[0] = TopologicalChargeMeta(U_f);

    //-----
    // Begin smearing (first smearing step from U_f to FineLatticeSmearedA)
    if (n_smear > 0)
    {
        // Apply smearing
        StoutSmearing(U_f, FineLatticeSmearedA, 0.2);
        // Calculate observables
        WilsonAction_f[1] = WilsonAction(FineLatticeSmearedA, beta) * action_norm_f;
        Pl_f[1] = Plaquette(FineLatticeSmearedA);
        Q_f[1] = TopologicalCharge(FineLatticeSmearedA);
        Q_meta_f[1] = TopologicalChargeMeta(FineLatticeSmearedA);
    }

    //-----
    // Further smearing steps
    for (int smear_count = 2; smear_count <= n_smear; ++smear_count)
    {
        // Even
        if (smear_count % 2 == 0)
        {
            // Apply smearing
            StoutSmearing(FineLatticeSmearedA, FineLatticeSmearedB, 0.2);
            // Calculate observables
            WilsonAction_f[smear_count] = WilsonAction(FineLatticeSmearedB, beta) * action_norm_f;
            Pl_f[smear_count] = Plaquette(FineLatticeSmearedB);
            Q_f[smear_count] = TopologicalCharge(FineLatticeSmearedB);
            Q_meta_f[smear_count] = TopologicalChargeMeta(FineLatticeSmearedB);
        }
        // Odd
        else
        {
            // Apply smearing
            StoutSmearing(FineLatticeSmearedB, FineLatticeSmearedA, 0.2);
            // Calculate observables
            WilsonAction_f[smear_count] = WilsonAction(FineLatticeSmearedA, beta) * action_norm_f;
            Pl_f[smear_count] = Plaquette(FineLatticeSmearedA);
            Q_f[smear_count] = TopologicalCharge(FineLatticeSmearedA);
            Q_meta_f[smear_count] = TopologicalChargeMeta(FineLatticeSmearedA);
        }
    }

    //-----
    // Calculate secondary observables from primary observables

    std::transform(Pl_f.begin(), Pl_f.end(), Pl_Re_f.begin(), [](const auto& element){return std::real(element);});
    std::transform(Pl_f.begin(), Pl_f.end(), Pl_Im_f.begin(), [](const auto& element){return std::imag(element);});

    //-----
    // Write to logfile
    datalog << "Step " << n_count << "\n";
    //-----
    datalog << "Action: ";
    std::copy(WilsonAction_f.cbegin(), std::prev(WilsonAction_f.cend()), std::ostream_iterator<T>(datalog, " "));
    datalog << WilsonAction_f.back() << "\n";
    //-----
    datalog << "Plaquette_Re: ";
    std::copy(Pl_Re_f.cbegin(), std::prev(Pl_Re_f.cend()), std::ostream_iterator<T>(datalog, " "));
    datalog << Pl_Re_f.back() << "\n";
    //-----
    datalog << "Plaquette_Im: ";
    std::copy(Pl_Im_f.cbegin(), std::prev(Pl_Im_f.cend()), std::ostream_iterator<T>(datalog, " "));
    datalog << Pl_Im_f.back() << "\n";
    //-----
    datalog << "TopCharge: ";
    std::copy(Q_f.cbegin(), std::prev(Q_f.cend()), std::ostream_iterator<T>(datalog, " "));
    datalog << Q_f.back() << "\n";
    //-----
    datalog << "TopChargeMeta: ";
    std::copy(Q_meta_f.cbegin(), std::prev(Q_meta_f.cend()), std::ostream_iterator<T>(datalog, " "));
    datalog << Q_meta_f.back() << "\n";
    //-----
    // If metadynamics updates are used, print the value of the bias potential at the current topological charge
    if constexpr(metadynamics_enabled)
    {
        datalog << "BiasPotential: " << BiasPotential.ReturnPotential(Q_meta_f[0]) << "\n" << endl;
    }
    else
    {
        datalog << endl;
    }
}

//----------------------------------------
// Coarse graining function for multiscale update

template<typename T, size_t Nx_f, size_t Nt_f, size_t Nx_c, size_t Nt_c>
void CoarseGrain(const GaugeField<T, Nx_f, Nt_f>& U_f, GaugeField<T, Nx_c, Nt_c>& U_c) noexcept
{
    // Coarse lattice links are given by the product of two fine lattice links
    // #pragma omp parallel for
    for (int t = 0; t < Nt_c; ++t)
    for (int x = 0; x < Nx_c; ++x)
    for (int mu = 0; mu < 2; ++mu)
    {
        U_c[t][x][mu] = U_f[2*t][2*x][mu] + U_f[2*t+1-mu][2*x+mu][mu];
    }
}

//----------------------------------------
// Fine graining function for multiscale update

template<typename T, size_t Nx_f, size_t Nt_f, size_t Nx_c, size_t Nt_c>
void FineGrain(GaugeField<T, Nx_f, Nt_f>& U_f, const GaugeField<T, Nx_c, Nt_c>& U_c) noexcept
{
    // First prolong links of the coarse lattice
    // #pragma omp parallel for
    for (int t = 0; t < Nt_c; ++t)
    for (int x = 0; x < Nx_c; ++x)
    for (int mu = 0; mu < 2; ++mu)
    {
        // Fine graining procedure 1
        // Set one fine lattice link equal to coarse lattice link, set the other link to unity
        // Preserves all observables on the coarse lattice
        U_f[2*t][2*x][mu] = U_c[t][x][mu];
        U_f[2*t+1-mu][2*x+mu][mu] = 0.0;
        // Fine graining procedure 2
        // Evenly distribute phase of coarse lattice link on fine lattice links
        // Also preserves all observables on the coarse lattice, but better translational symmetry?
        // U_f[2*t][2*x][mu] = U_c[t][x][mu] * 0.5;
        // U_f[2*t+1-mu][2*x+mu][mu] = U_c[t][x][mu] * 0.5;
        // Set remaining links to unity
        U_f[2*t+1-mu][2*x+mu][1-mu] = 0.0;
        U_f[2*t+1][2*x+1][mu] = 0.0;
    }
    // Afterwards, use APE smearing to supress UV fluctuations by smoothing the links set to unity in the previous step
    for (int smooth_count = 0; smooth_count < n_smooth; ++smooth_count)
    {
        // #pragma omp parallel for
        for (int t = 0; t < Nt_c; ++t)
        for (int x = 0; x < Nx_c; ++x)
        for (int mu = 0; mu < 2; ++mu)
        {
            U_f[2*t+1-mu][2*x+mu][1-mu] = APESmearingSingle(U_f, alpha_smooth, 2*t+1-mu, 2*x+mu, 1-mu);
            U_f[2*t+1][2*x+1][mu] = APESmearingSingle(U_f, alpha_smooth, 2*t+1, 2*x+1, mu);
        }
    }
}

//----------------------------------------

int main()
{
    // iostream not synchronized with corresponding C streams, might cause a problem with C libraries and might not be thread safe
    std::ios_base::sync_with_stdio(false);
    cout << std::setprecision(12) << std::fixed;
    datalog << std::setprecision(12) << std::fixed;
    datalog_flowed << std::setprecision(12) << std::fixed;
    multiscalelog << std::setprecision(12) << std::fixed;

    // constexpr UpdateScheme PureMeta{.multiscale_enabled = false, .metadynamics_enabled = true};
    // constexpr UpdateScheme PureMultiscale{.multiscale_enabled = true, .metadynamics_enabled = false};
    // constexpr UpdateScheme CombinedMetaMultiscale{.multiscale_enabled = true, .metadynamics_enabled = true};
    // constexpr UpdateScheme CustomUpdateScheme{.multiscale_enabled = multiscale_enabled, .metadynamics_enabled = metadynamics_enabled};

    // Parameters for fine lattice
    double epsilon_f {0.2};
    AcceptanceRates AcceptanceRates_f{.metro_acceptance = 0, .top_acceptance = 0};
    // Parameters for coarse lattice
    double epsilon_c {0.4};
    AcceptanceRates AcceptanceRates_c{.metro_acceptance = 0, .top_acceptance = 0};

    std::uniform_real_distribution<double> distribution_prob(0, 1);
    std::uniform_int_distribution<int> distribution_int(0, 1);

    Configuration();
    prng_vector = CreatePRNGs();

    CreateFiles();
    InstantonStart(FineLattice, 0);
    // HotStart(CoarseLattice);
    auto startcalc = std::chrono::system_clock::now();
    datalog.open(logfilepath, std::fstream::out | std::fstream::app);
    datalog_flowed.open(flowfilepath, std::fstream::out | std::fstream::app);
    if constexpr(multiscale_enabled)
    {
        multiscalelog.open(multiscalefilepath, std::fstream::out | std::fstream::app);
    }

    //-----
    // Metadynamics

    if constexpr(metadynamics_enabled)
    {
        // MetaBiasPotential TopBiasPotential{.Q_min = -40.5, .Q_max = 40.5, .bin_number = 100000, .weight = 0.0001, .threshold_weight = 20.0};
        MetaBiasPotential TopBiasPotential{-40.5, 40.5, 50000, 0.0001, 1000.0};
        // MetaBiasPotential TopBiasPotential{-12.0, 12.0, 20000, 0.0001, 1000.0};
        // TopBiasPotential.AddPenaltyWeight(-10.0, 10.0, penaltypotentialfilepath);

        // Home PC
        // TopBiasPotential.LoadPotential("meta_potentials/metapotential.txt");
        // Cluster
        // TopBiasPotential.LoadPotential("metapotential_metro/metapotential.txt");
        // TopBiasPotential.LoadPotential("metapotential_combined/metapotential.txt");
        //-----
        // TopBiasPotential.LoadPotential("Schwinger_N=32x32_beta=12.800000 (46)/metapotential.txt");
        TopBiasPotential.SaveMetaParameters(metapotentialfilepath);

        // Explicitly perform thermalization here
        std::uniform_real_distribution<double> distribution_phase(-epsilon_f, epsilon_f);
        for (int count_therm = 0; count_therm < 1000; ++count_therm)
        {
            MetropolisUpdate(FineLattice, beta, AcceptanceRates_f.metro_acceptance, distribution_prob, distribution_phase);
        }
        for (int count = 0; count < n_run_total; ++count)
        {
            Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
            if (count % expectation_period == 0)
            {
                Observables(FineLattice, CoarseLattice, TopBiasPotential, beta, n_smear, datalog, count);
                if constexpr(metapotential_updated)
                {
                    if (count % (100 * expectation_period) == 0)
                    {
                        TopBiasPotential.SaveMetaPotential(metapotentialfilepath);
                    }
                }
            }
        }
        if constexpr(!metapotential_updated)
        {
            TopBiasPotential.SaveMetaPotential(metapotentialfilepath);
        }
    }

    //-----
    // Test if the topological charge is preserved under fine- and coarse-graining
    // TestMultiscale_Instanton(FineLattice, CoarseLattice, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
    // TestMultiscale(FineLattice, CoarseLattice, beta, epsilon_f, epsilon_c, AcceptanceRates_f, AcceptanceRates_c, distribution_prob, distribution_int);

    //-----
    // Multiscale

    if constexpr(multiscale_enabled)
    {
        MetaBiasPotential TopBiasPotential{-1.0, 1.0, 10, 0.1, 1.0};
        for (int n_count_c = 0; n_count_c < n_run_c; ++n_count_c)
        {
            Update(CoarseLattice, TopBiasPotential, beta/4.0, epsilon_c, AcceptanceRates_c, distribution_prob, distribution_int);
            // epsilon_c += (AcceptanceRates_c.metro_acceptance * update_norm_c - 0.8) * 0.2;
            if (n_count_c % expectation_period == 0)
            {
                FineGrain(FineLattice, CoarseLattice);
                for (int n_count_f = 0; n_count_f < n_run_f; ++n_count_f)
                {
                    Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
                    // epsilon_f += (AcceptanceRates_f.metro_acceptance * update_norm_f - 0.8) * 0.2;
                }
                Observables(FineLattice, CoarseLattice, TopBiasPotential, beta, n_smear, datalog, n_count_c);
            }
        }
    }
    // for (int n_count_total = 0; n_count_total < n_run_total; ++n_count_total)
    // {
    //     //-----
    //     // Updates on coarse lattice
    //     if constexpr(multiscale_enabled)
    //     {
    //         for (int n_count_c = 0; n_count_c < n_run_c; ++n_count_c)
    //         {
    //             Update(CoarseLattice, TopBiasPotential, beta/4.0, epsilon_c, AcceptanceRates_c, distribution_prob, distribution_int);
    //             epsilon_c += (AcceptanceRates_c.metro_acceptance * update_norm_c - 0.8) * 0.2;
    //         }
    //         //-----
    //         // Fine-grain coarse lattice to fine lattice
    //         FineGrain(FineLattice, CoarseLattice);
    //     }
    //     for (int n_count_f = 0; n_count_f < 100; ++n_count_f)
    //     {
    //         Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
    //     }
    //     //-----
    //     // Updates on fine lattice
    //     for (int n_count_f = 0; n_count_f < n_run_f; ++n_count_f)
    //     {
    //         Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
    //         epsilon_f += (AcceptanceRates_f.metro_acceptance * update_norm_f - 0.8) * 0.2;
    //         if (n_count_f % expectation_period == 0)
    //         {
    //             // ResetPhase(FineLattice);
    //             Observables(FineLattice, CoarseLattice, TopBiasPotential, beta, n_smear, datalog, n_count_f + n_count_total * n_run_f);
    //         }
    //     }
    //     if constexpr(multiscale_enabled)
    //     {
    //         //-----
    //         // Coarse-grain from fine lattice to coarse lattice
    //         CoarseGrain(FineLattice, CoarseLattice);
    //     }
    // }

    //-----
    // Calculate observables in topological sectors
    // MetaBiasPotential TopBiasPotential{-1.0, 1.0, 10, 0.1, 1.0};
    // for (int Q = 0; Q <= 0; ++Q)
    // {
    //     CreateFiles();
    //     datalog.open(logfilepath, std::fstream::out | std::fstream::app);
    //     InstantonStart(FineLattice, Q);
    //     for (int count = 0; count < n_run_total; ++count)
    //     {
    //         Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
    //         if (count % expectation_period == 0)
    //         {
    //             Observables(FineLattice, CoarseLattice, TopBiasPotential, beta, n_smear, datalog, count);
    //         }
    //     }
    //     datalog.close();
    //     datalog.clear();
    // }

    //-----
    // No metadynamics and no multiscale

    MetaBiasPotential TopBiasPotential{-1.0, 1.0, 10, 0.1, 1.0};
    for (int count = 0; count < n_run_total; ++count)
    {
        Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
        if (count % expectation_period == 0)
        {
            Observables(FineLattice, CoarseLattice, TopBiasPotential, beta, n_smear, datalog, count);
        }
    }

    //-----
    // Wilson flow tests

    // MetaBiasPotential TopBiasPotential{-1.0, 1.0, 10, 0.1, 1.0};
    // int n_flow {400};
    // double jacobian_forward {0.0};
    // double jacobian_backward {0.0};
    // double flow_epsilon {0.01};
    // int top_acceptance0 {0};
    // int top_acceptance1 {0};
    // int top_acceptance2 {0};
    // int top_acceptance3 {0};
    // int top_acceptance4 {0};
    // int top_acceptance5 {0};
    // int top_acceptance6 {0};
    // // for (int count = 0; count < 100; ++count){Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);}
    // for (int count = 0; count < n_run_total; ++count)
    // {
    //     Update(FineLattice, TopBiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
    //     // HotStart(FineLattice);
    //     // Test topological update + Wilson flow
    //     int DeltaQ = distribution_int(generator_rand) * 2 - 1;
    //     datalog_flowed << "DeltaQ: " << DeltaQ << endl;
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 0, top_acceptance0, distribution_prob, DeltaQ) << " ";
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 50, top_acceptance1, distribution_prob, DeltaQ) << " ";
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 100, top_acceptance2, distribution_prob, DeltaQ) << " ";
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 150, top_acceptance3, distribution_prob, DeltaQ) << " ";
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 200, top_acceptance4, distribution_prob, DeltaQ) << " ";
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 300, top_acceptance5, distribution_prob, DeltaQ) << " ";
    //     FineLatticeCopy = FineLattice;
    //     datalog_flowed << TopologicalUpdateFlow<Order::ForwardsSeq, WilsonFlowScheme::Stout>(FineLatticeCopy, beta, flow_epsilon, 400, top_acceptance6, distribution_prob, DeltaQ) << endl;
    //     if (count % expectation_period == 0)
    //     {
    //         // jacobian_forward = 0.0;
    //         // jacobian_backward = 0.0;
    //         // FineLatticeFlowed = FineLattice;
    //         // std::cout << "Forward:\n" << Plaquette(FineLatticeFlowed) << "\n";
    //         // for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    //         // {
    //         //     WilsonFlow<Order::BackwardsSeq, WilsonFlowScheme::MidpointImplicit>(FineLatticeFlowed, flow_epsilon);
    //         //     std::cout << "Forward:\n" << Plaquette(FineLatticeFlowed) << "\n";
    //         //     jacobian_forward += std::real(Plaquette(FineLatticeFlowed));
    //         // }
    //         // std::cout << "Backward:\n" << Plaquette(FineLatticeFlowed) << "\n";
    //         // for (int flow_count = 0; flow_count < n_flow; ++flow_count)
    //         // {
    //         //     WilsonFlow<Order::BackwardsSeq, WilsonFlowScheme::MidpointImplicit>(FineLatticeFlowed, -flow_epsilon);
    //         //     std::cout << "Backward:\n" << Plaquette(FineLatticeFlowed) << "\n";
    //         //     jacobian_backward += std::real(Plaquette(FineLatticeFlowed));
    //         // }
    //         // jacobian_forward *= -16.0/3.0 * flow_epsilon;
    //         // jacobian_backward *= 16.0/3.0 * flow_epsilon;
    //         // cout << "jacobian_forward: " << jacobian_forward << "\n";
    //         // cout << "jacobian_backward: " << jacobian_backward << "\n";
    //         // cout << jacobian_forward + jacobian_backward << "\n" << endl;
    //         Observables(FineLattice, CoarseLattice, TopBiasPotential, beta, n_smear, datalog, count);
    //         // Observables(FineLatticeFlowed, CoarseLattice, TopBiasPotential, beta, n_smear, datalog_flowed, count);
    //     }
    // }

    // datalog_flowed << "AcceptanceRate (t = 0): " << top_acceptance0 << "\n";
    // datalog_flowed << "AcceptanceRate (t = 0.5): " << top_acceptance1 << "\n";
    // datalog_flowed << "AcceptanceRate (t = 1): " << top_acceptance2 << "\n";
    // datalog_flowed << "AcceptanceRate (t = 1.5): " << top_acceptance3 << "\n";
    // datalog_flowed << "AcceptanceRate (t = 2): " << top_acceptance4 << "\n";
    // datalog_flowed << "AcceptanceRate (t = 3): " << top_acceptance5 << "\n";
    // datalog_flowed << "AcceptanceRate (t = 4): " << top_acceptance6 << "\n" << endl;

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - startcalc;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);

    if constexpr(multiscale_enabled)
    {
        // PrintFinal(multiscalelog, AcceptanceRates_f, AcceptanceRates_c, epsilon_f, epsilon_c, end_time, elapsed_seconds);
        multiscalelog.close();
        multiscalelog.clear();
    }

    //-----
    // Print acceptance rates, PRNG widths, and required time to terminal and to logfile

    PrintFinal(cout, AcceptanceRates_f, AcceptanceRates_c, epsilon_f, epsilon_c, end_time, elapsed_seconds);
    //-----
    PrintFinal(datalog, AcceptanceRates_f, AcceptanceRates_c, epsilon_f, epsilon_c, end_time, elapsed_seconds);
    datalog.close();
    datalog.clear();
    //-----
    PrintFinal(datalog_flowed, AcceptanceRates_f, AcceptanceRates_c, epsilon_f, epsilon_c, end_time, elapsed_seconds);
    datalog_flowed.close();
    datalog_flowed.clear();
}
