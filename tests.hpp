#ifndef LETTUCE_TESTS_MULTISCALE_HPP
#define LETTUCE_TESTS_MULTISCALE_HPP

// Non-standard library headers
#include "dressing.hpp"
#include "smearing.hpp"
//----------------------------------------
// Standard library headers
// ...
//----------------------------------------
// Standard C++ headers
#include <algorithm>
#include <iostream>
// #include <string>
#include <vector>
//----------------------------------------
// Standard C headers
// #include <cmath>

//----------------------------------------
// Test if the topological charge of instanton configurations is preserved under fine-graining and coarse-graining

template<typename T, size_t Nx_f, size_t Nt_f, size_t Nx_c, size_t Nt_c>
void TestMultiscale_Instanton(GaugeField<T, Nx_f, Nt_f>& U_f, GaugeField<T, Nx_c, Nt_c>& U_c, MetaBiasPotential<T>& BiasPotential, const T beta, const T epsilon_f, AcceptanceRates& AcceptanceRates, std::uniform_real_distribution<T>& distribution_prob, std::uniform_int_distribution<int>& distribution_int) noexcept
{
    std::cout << "Testing multiscale algorithm...\n\n";
    std::cout << "Preservation of topological charge of instanton configurations after coarse- and fine graining:\n";
    for (int Q = -10; Q <= 10; ++Q )
    {
        std::cout << "\n\nInstanton configuration with Q = " << Q << ":\n";
        InstantonStart(U_f, Q);
        std::cout << "Q (Fine Lattice): " << TopologicalCharge(U_f) << "\n";
        //-----
        // Coarse Lattice
        CoarseGrain(U_f, U_c);
        std::cout << "Q (Coarse Lattice): " << TopologicalCharge(U_c) << "\n";
        // 1 Smearing step
        APESmearing(U_c, CoarseLatticeSmearedA, alpha_APE);
        std::cout << "Q (Coarse Lattice, 1): " << TopologicalCharge(CoarseLatticeSmearedA) << "\n";
        // 2 Smearing steps
        APESmearing(CoarseLatticeSmearedA, CoarseLatticeSmearedB, alpha_APE);
        std::cout << "Q (Coarse Lattice, 2): " << TopologicalCharge(CoarseLatticeSmearedB) << "\n";
        // 3 Smearing steps
        APESmearing(CoarseLatticeSmearedB, CoarseLatticeSmearedA, alpha_APE);
        std::cout << "Q (Coarse Lattice, 3): " << TopologicalCharge(CoarseLatticeSmearedA) << "\n";
        //-----
        // Fine Lattice
        FineGrain(U_f, U_c);
        std::cout << "Q (Fine Lattice): " << TopologicalCharge(U_f) << "\n";
        // 1 Smearing step
        APESmearing(U_f, FineLatticeSmearedA, alpha_APE);
        std::cout << "Q (Fine Lattice, 1): " << TopologicalCharge(FineLatticeSmearedA) << "\n";
        // 2 Smearing steps
        APESmearing(FineLatticeSmearedA, FineLatticeSmearedB, alpha_APE);
        std::cout << "Q (Fine Lattice, 2): " << TopologicalCharge(FineLatticeSmearedB) << "\n";
        // 3 Smearing steps
        APESmearing(FineLatticeSmearedB, FineLatticeSmearedA, alpha_APE);
        std::cout << "Q (Fine Lattice, 3): " << TopologicalCharge(FineLatticeSmearedA) << "\n";
    }
    // for (int i = 0; i < 100; ++i)
    // {
    //     for (int count = 0; count < 500; ++count)
    //     {
    //         Update(U_f, BiasPotential, beta, epsilon_f, AcceptanceRates, distribution_prob, distribution_int);
    //         epsilon_f += (AcceptanceRates.metro_acceptance * update_norm_f - 0.8) * 0.2;
    //     }
    //     std::cout << "Q (Fine Lattice): " << TopologicalCharge(U_f) << "\n";
    //     CoarseGrain(U_f, U_c);
    //     std::cout << "Q (Coarse Lattice): " << TopologicalCharge(U_c) << "\n";
    //     FineGrain(U_f, U_c);
    //     std::cout << "Q (Fine Lattice): " << TopologicalCharge(U_f) << "\n" << std::endl;
    //     ResetPhase(U_f);
    // }
}

//----------------------------------------
// Test if the topological charge of random configurations (generated from a normal update procedure) is preserved under fine-graining and coarse-graining

template<typename T, size_t Nx_f, size_t Nt_f, size_t Nx_c, size_t Nt_c>
void TestMultiscale(GaugeField<T, Nx_f, Nt_f>& U_f, GaugeField<T, Nx_c, Nt_c>& U_c, const T beta, const T epsilon_f, const T epsilon_c, AcceptanceRates& AcceptanceRates_f, AcceptanceRates& AcceptanceRates_c, std::uniform_real_distribution<T>& distribution_prob, std::uniform_int_distribution<int>& distribution_int) noexcept
{
    // Placeholder bias potential
    MetaBiasPotential BiasPotential{-1.0, 1.0, 10, 0.1, 1.0};

    // std::cout << "Testing multiscale algorithm...\n\n";
    // std::cout << "Preservation of topological charge of instanton configurations after coarse- and fine graining:\n";
    // for (int Q = -10; Q <= 10; ++Q)
    // {
    //     InstantonStart(U_f, Q);
    //     std::cout << "Q (Fine Lattice, Instanton): " << TopologicalCharge(U_f) << "\n";
    //     for (int i = 0; i < 1000; ++i)
    //     {
    //         Update(U_f, BiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
    //     }
    //     std::cout << "Q (Fine Lattice): " << TopologicalCharge(U_f) << "\n";
    //     CoarseGrain(U_f, U_c);
    //     std::cout << "Q (Coarse Lattice): " << TopologicalCharge(U_c) << "\n";
    //     FineGrain(U_f, U_c);
    //     std::cout << "Q (Fine Lattice): " << TopologicalCharge(U_f) << "\n\n";
    // }

    // Generate random coarse configuration and perform updates
    HotStart(U_c);
    for (int n_count_c = 0; n_count_c < n_run_c; ++n_count_c)
    {
        Update(U_c, BiasPotential, beta/4.0, epsilon_c, AcceptanceRates_c, distribution_prob, distribution_int);
        if (n_count_c % expectation_period == 0)
        {
            FineGrain(U_f, U_c);
            std::vector<T> Q_f(n_smear + 1);
            std::vector<T> Q_c(n_smear + 1);
            std::vector<T> Q_f_updated(n_smear + 1);
            Q_f[0] = TopologicalCharge(U_f);
            Q_c[0] = TopologicalCharge(U_c);
            //-----
            // Begin smearing (first smearing step from U_f to FineLatticeSmearedA)
            if (n_smear > 0)
            {
                // Apply smearing
                APESmearing(U_f, FineLatticeSmearedA, alpha_APE);
                APESmearing(U_c, CoarseLatticeSmearedA, alpha_APE);
                // Calculate observables
                Q_f[1] = TopologicalCharge(FineLatticeSmearedA);
                Q_c[1] = TopologicalCharge(CoarseLatticeSmearedA);
            }

            //-----
            // Further smearing steps
            for (int smear_count = 2; smear_count <= n_smear; ++smear_count)
            {
                // Even
                if (smear_count % 2 == 0)
                {
                    // Apply smearing
                    APESmearing(FineLatticeSmearedA, FineLatticeSmearedB, alpha_APE);
                    APESmearing(CoarseLatticeSmearedA, CoarseLatticeSmearedB, alpha_APE);
                    // Calculate observables
                    Q_f[smear_count] = TopologicalCharge(FineLatticeSmearedB);
                    Q_c[smear_count] = TopologicalCharge(CoarseLatticeSmearedB);

                }
                // Odd
                else
                {
                    // Apply smearing
                    APESmearing(FineLatticeSmearedB, FineLatticeSmearedA, alpha_APE);
                    APESmearing(CoarseLatticeSmearedB, CoarseLatticeSmearedA, alpha_APE);
                    // Calculate observables
                    Q_f[smear_count] = TopologicalCharge(FineLatticeSmearedA);
                    Q_c[smear_count] = TopologicalCharge(CoarseLatticeSmearedA);
                }
            }
            //-----
            // Update fine lattice and do the same thing again
            for (int n_count_f = 0; n_count_f < n_run_f; ++n_count_f)
            {
                Update(U_f, BiasPotential, beta, epsilon_f, AcceptanceRates_f, distribution_prob, distribution_int);
            }
            //-----
            // Begin smearing (first smearing step from U_f to FineLatticeSmearedA)
            if (n_smear > 0)
            {
                // Apply smearing
                APESmearing(U_f, FineLatticeSmearedA, alpha_APE);
                // Calculate observables
                Q_f_updated[0] = TopologicalCharge(U_f);
                Q_f_updated[1] = TopologicalCharge(FineLatticeSmearedA);
            }

            //-----
            // Further smearing steps
            for (int smear_count = 2; smear_count <= n_smear; ++smear_count)
            {
                // Even
                if (smear_count % 2 == 0)
                {
                    // Apply smearing
                    APESmearing(FineLatticeSmearedA, FineLatticeSmearedB, alpha_APE);
                    // Calculate observables
                    Q_f_updated[smear_count] = TopologicalCharge(FineLatticeSmearedB);

                }
                // Odd
                else
                {
                    // Apply smearing
                    APESmearing(FineLatticeSmearedB, FineLatticeSmearedA, alpha_APE);
                    // Calculate observables
                    Q_f_updated[smear_count] = TopologicalCharge(FineLatticeSmearedA);
                }
            }
            //-----
            // Write to logfile
            multiscalelog << "Step " << n_count_c << "\n";
            //-----
            multiscalelog << "TopCharge_c: ";
            std::copy(Q_c.cbegin(), std::prev(Q_c.cend()), std::ostream_iterator<T>(multiscalelog, " "));
            multiscalelog << Q_c.back() << "\n";
            //-----
            multiscalelog << "TopCharge_f: ";
            std::copy(Q_f.cbegin(), std::prev(Q_f.cend()), std::ostream_iterator<T>(multiscalelog, " "));
            multiscalelog << Q_f.back() << "\n";
            //-----
            multiscalelog << "TopCharge_f_updated: ";
            std::copy(Q_f_updated.cbegin(), std::prev(Q_f_updated.cend()), std::ostream_iterator<T>(multiscalelog, " "));
            multiscalelog << Q_f_updated.back() << "\n" << std::endl;
        }
    }
}

#endif
