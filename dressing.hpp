#ifndef LETTUCE_DRESSING_HPP
#define LETTUCE_DRESSING_HPP

// Non-standard library headers
#include "ansi_colors.hpp"
#include "pcg_random.hpp"
//----------------------------------------
// Standard library headers
// ...
//----------------------------------------
// Standard C++ headers
#include <array>
#include <chrono>
#include <complex>
#include <fstream>
#include <random>
#include <string>
#include <vector>
// #include <utility>
//----------------------------------------
// Standard C headers
// ...

//-------------------------------------------------------------------------------------

std::string program_version = "Schwinger_version_0.4";

//----------------------------------------
// Simulation parameters
inline constexpr int Nt {30};                               // Size of the finest lattice in temporal direction
inline constexpr int Nx {30};                               // Size of the finest lattice in spatial direction
double beta;                                                // Initial coupling constant of the finest lattice
int n_run_total;                                            // Total number of runs (how many times to perform fine and coarse lattice updates)
int n_run_f;                                                // Number of runs/update routines for the fine lattice
int n_run_c;                                                // Number of runs/update routines for the coarse lattice
int expectation_period;                                     // Number of updates between calculation of expectation values
inline constexpr int multi_hit {8};                         // Number of hits per site in Metropolis algorithm
inline constexpr bool metadynamics_enabled {false};         // Enable metadynamics updates or not
inline constexpr bool metapotential_updated {false};        // If true, update the metapotential with every update, if false, simulate with a static metapotential
inline constexpr bool multiscale_enabled {false};           // Enable multiscale updates or not
bool top_update;                                            // Enable topological update or not
inline constexpr int n_smear {3};                           // How often to apply smearing during the measurement of observables
inline constexpr double alpha_APE {0.5};                    // APE smearing parameter
inline constexpr double alpha_smooth {0.05};                // APE smearing parameter for multisclae smoothing after fine graining
inline constexpr int n_smooth {150};                        // How often to apply smearing after fine graining
// inline constexpr double rho_stout {0.15};                   // Stout smearing parameter
//----------------------------------------
// Constants and variables derived from simulation parameters
double n_run_f_inverse;                                     // Inverse number of fine runs
double n_run_c_inverse;                                     // Inverse number of coarse runs
double update_norm_f = 1.0 / (Nt * Nx * 2.0 * multi_hit);
double full_norm_f = 1.0 / (Nt * Nx);
double update_norm_c = 1.0 / (Nt/2 * Nx/2 * 2.0 * multi_hit);
double full_norm_c = 1.0 / (Nt/2 * Nx/2);
// double spatial_norm = 1.0 / (Nx);
//----------------------------------------
// Variables used during creation of files and folders
auto start = std::chrono::system_clock::now();              // Start time
std::string logfilepath;                                    // Filepath (log)
std::string parameterfilepath;                              // Filepath (parameters)
std::string multiscalefilepath;                             // Filepath (Multiscale log)
std::string metapotentialfilepath;                          // Filepath (Metadynamics potential)
std::string penaltypotentialfilepath;                       // Filepath (Metadynamics penalty potential)
std::string flowfilepath;                                   // Filepath (Wilson flow)
//----------------------------------------
// Filestreams for logging data
std::ofstream datalog;                                      // Output stream to save data
std::ofstream datalog_flowed;                               // Output stream to save data (Wilson flow testing)
std::ofstream multiscalelog;                                // Output stream to save data (multiscale specific)
//----------------------------------------
// PRNG stuff
pcg_extras::seed_seq_from<std::random_device> seed_source;  // Seed source to seed PRNGs
#ifdef FIXED_SEED                                           // PRNG for everything but the parallel updates
pcg64 generator_rand(1);
#else
pcg64 generator_rand(seed_source);
#endif
std::vector<pcg64> prng_vector;                             // Vector of PRNGs for parallel usage

//----------------------------------------

template<typename T>
inline constexpr std::complex<T> i(0, 1);
template<typename T>
inline constexpr T pi = static_cast<T>(3.14159265358979323846L);

//----------------------------------------

template<typename T, size_t Nx, size_t Nt>
using GaugeField = std::array<std::array<std::array<T, 2>, Nx>, Nt>;

//----------------------------------------
// Define lattices
GaugeField<double, Nx, Nt> FineLattice;
GaugeField<double, Nx, Nt> FineLatticeCopy;
GaugeField<double, Nx/2, Nt/2> CoarseLattice;
// Define lattices for smearing
GaugeField<double, Nx, Nt> FineLatticeSmearedA;
GaugeField<double, Nx, Nt> FineLatticeSmearedB;
GaugeField<double, Nx/2, Nt/2> CoarseLatticeSmearedA;
GaugeField<double, Nx/2, Nt/2> CoarseLatticeSmearedB;
// Define lattice for Wilson flow
GaugeField<double, Nx, Nt> FineLatticeFlowed;

//----------------------------------------
// Struct containing information about the update scheme

struct UpdateScheme
{
    bool multiscale_enabled;
    bool metadynamics_enabled;
};

//----------------------------------------
// Struct containing acceptance rates of Metropolis and topological update

struct AcceptanceRates
{
    uint_fast64_t metro_acceptance;
    uint_fast64_t top_acceptance;
};

//----------------------------------------
// Enum to determine the order in which to go through the lattice (for example during updating)

enum class Order {ForwardsSeq, BackwardsSeq, ForwardsCheckerPar, BackwardsCheckerPar};

//----------------------------------------
// Enum to determine integration scheme used in Wilson flow

enum class WilsonFlowScheme {APE, Stout, StoutNew, EulerImplicit, EulerImplicitNew, MidpointImplicit, RK4};

//----------------------------------------
// Trick to allow type promotion below
template<typename T>
struct identity_t { typedef T type; };

// Make working with std::complex<> numbers suck less... allow promotion.
#define COMPLEX_OPS(OP)                                                 \
  template<typename _Tp>                                                \
  std::complex<_Tp>                                                     \
  operator OP(std::complex<_Tp> lhs, const typename identity_t<_Tp>::type & rhs) \
  {                                                                     \
    return lhs OP rhs;                                                  \
  }                                                                     \
  template<typename _Tp>                                                \
  std::complex<_Tp>                                                     \
  operator OP(const typename identity_t<_Tp>::type & lhs, const std::complex<_Tp> & rhs) \
  {                                                                     \
    return lhs OP rhs;                                                  \
  }
COMPLEX_OPS(+)
COMPLEX_OPS(-)
COMPLEX_OPS(*)
COMPLEX_OPS(/)
#undef COMPLEX_OPS

#endif
