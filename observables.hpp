#ifndef LETTUCE_OBSERVABLES_HPP
#define LETTUCE_OBSERVABLES_HPP

// Non-standard library headers
#include "dressing.hpp"
//----------------------------------------
// Standard library headers
#include <omp.h>
//----------------------------------------
// Standard C++ headers
#include <complex>
#include <string>
#include <utility>
//----------------------------------------
// Standard C headers
#include <cmath>

//-------------------------------------------------------------------------------------

//----------------------------------------
// Calculates the plaquette average over
// the whole lattice
//----------------------------------------

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
std::complex<floatT> Plaquette(const GaugeField<floatT, Nx, Nt>& U) noexcept
{
    std::complex<floatT> Pl {0.0, 0.0};
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        Pl += std::exp(i<floatT> * (U[t][x][0] + U[(t+1)%Nt][x][1] - U[t][(x+1)%Nx][0] - U[t][x][1]));
    }
    return Pl/(Nt * Nx);
}

//----------------------------------------
// Calculates a single plaquette at coordinates t and x in (0,1)/(t,x)-direction, i.e., P_{0,1}(t,x)

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
std::complex<floatT> PlaquetteSingle(const GaugeField<floatT, Nx, Nt>& U, const int t, const int x) noexcept
{
    return std::exp(i<floatT> * (U[t][x][0] + U[(t+1)%Nt][x][1] - U[t][(x+1)%Nx][0] - U[t][x][1]));
}

//----------------------------------------
// Calculates the staple at coordinates t and x in mu-direction in two dimensions

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
std::complex<floatT> Staple(const GaugeField<floatT, Nx, Nt>& U, const int t, const int x, const int mu) noexcept
{
    int nu {1 - mu};
    std::complex<floatT> staple_1, staple_2;
    staple_1 = std::exp(i<floatT> * (U[t][x][nu] + U[(t+mu)%Nt][(x+nu)%Nx][mu] - U[(t+nu)%Nt][(x+mu)%Nx][nu]));
    staple_2 = std::exp(i<floatT> * (-U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] + U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] + U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu]));
    return staple_1 + staple_2;
}


//----------------------------------------
// Calculates the daggered staple at coordinates t and x in mu-direction in two dimensions

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
std::complex<floatT> StapleDaggered(const GaugeField<floatT, Nx, Nt>& U, const int t, const int x, const int mu) noexcept
{
    int nu {1 - mu};
    std::complex<floatT> staple_1, staple_2;
    staple_1 = std::exp(i<floatT> * (-U[t][x][nu] - U[(t+mu)%Nt][(x+nu)%Nx][mu] + U[(t+nu)%Nt][(x+mu)%Nx][nu]));
    staple_2 = std::exp(i<floatT> * (U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][nu] - U[(t-mu+Nt)%Nt][(x-nu+Nx)%Nx][mu] - U[(t+1-2*mu+Nt)%Nt][(x+2*mu-1+Nx)%Nx][nu]));
    return staple_1 + staple_2;
}

//----------------------------------------
// Calculates the Wilson action

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
floatT WilsonAction(const GaugeField<floatT, Nx, Nt>& U, const floatT beta) noexcept
{
    floatT Action {0.0};
    // #pragma omp parallel for reduction(+:Action)
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        Action += 1.0 - std::real(PlaquetteSingle(U, t, x));
        // Action += 1.0 - std::cos(i<floatT> * (U[t][x][0] + U[(t + 1)%Nt][x][1] - U[t][(x + 1)%Nx][0] - U[t][x][1]));
    }
    Action *= beta;
    return Action;
}

//----------------------------------------
// Calculates the topological charge in two dimensions

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
floatT TopologicalCharge(const GaugeField<floatT, Nx, Nt>& U) noexcept
{
    floatT TopCharge {0.0};
    // #pragma omp parallel for reduction(+:TopCharge)
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        // TopCharge += std::fmod(std::fmod(U[t][x][0] + U[(t + 1)%Nt][x][1] - U[t][(x + 1)%Nx][0] - U[t][x][1], 2.0 * pi<floatT>) + 3.0 * pi<floatT>, 2.0 * pi<floatT>) - pi<floatT>;
        TopCharge += std::imag(std::log(std::exp(i<floatT> * (U[t][x][0] + U[(t + 1)%Nt][x][1] - U[t][(x + 1)%Nx][0] - U[t][x][1]))));
        // TODO: Check if log exp definition is faster
        // cout << "TopCharge of Plaq: " << std::fmod(std::fmod(U[t][x][0] + U[(t + 1)%Nt][x][1] - U[t][(x + 1)%Nx][0] - U[t][x][1], 2.0 * pi<floatT>) + 3.0 * pi<floatT>, 2.0 * pi<floatT>) - pi<floatT> << endl;
        // cout << "TopCharge of Plaq: " << std::imag(std::log(std::exp(i<floatT> * (U[t][x][0] + U[(t + 1)%Nt][x][1] - U[t][(x + 1)%Nx][0] - U[t][x][1])))) << "\n" << endl;
    }
    TopCharge *= 1.0/(2.0 * pi<floatT>);
    return TopCharge;
}

//----------------------------------------
// Calculates a non-integer version of the topological charge in two dimensions (used for metadynamics histogram)

template<typename floatT, size_t Nx, size_t Nt>
[[nodiscard]]
floatT TopologicalChargeMeta(const GaugeField<floatT, Nx, Nt>& U) noexcept
{
    floatT TopChargeMeta {0.0};
    // #pragma omp parallel for reduction(+:TopChargeMeta)
    for (size_t t = 0; t < Nt; ++t)
    for (size_t x = 0; x < Nx; ++x)
    {
        TopChargeMeta += std::sin(U[t][x][0] + U[(t + 1)%Nt][x][1] - U[t][(x + 1)%Nx][0] - U[t][x][1]);
    }
    TopChargeMeta *= 1.0/(2.0 * pi<floatT>);
    return TopChargeMeta;
}

#endif
